## Marktrecherche und Alleinstellungsmerkmal

Kompetenzmanagement stellt eine wichtige Aufgabe für viele
Beratungsunternehmen dar. Auf dem Markt gibt es schon Produkte, die sich
dessen annehmen, doch oft sind die Lösungen sehr umfassend und entfernen
sich von dem Wesentlichen. In der Nachfolgenden Tabelle stehen die
beiden Systeme “SkillCert” und “rexxsystems” im Vergleich zu unserer
Anwendung “SMS”.

\pagebreak

+-------------------------------------------------+-----------+-------------+-----+
| Funktionen                                      | SkillCert | rexxsystems | SMS |
+=================================================+===========+=============+=====+
| Verwalten der Kompetenzarten                    | X         | X           | X   |
+-------------------------------------------------+-----------+-------------+-----+
| Pflegen der Mitarbeiterprofile                  | X         | X           | X   |
+-------------------------------------------------+-----------+-------------+-----+
| Pflegen des Projektstatus der Consultants       |           |             | X   |
+-------------------------------------------------+-----------+-------------+-----+
| Suchen von Mitarbeitern nach Kompetenzkriterien | X         | X           | X   |
+-------------------------------------------------+-----------+-------------+-----+
| Generieren einer Mitarbeiterübersicht           | X         | X           | X   |
+-------------------------------------------------+-----------+-------------+-----+
| Pflegen der Projektdaten                        | X         | X           | X   |
+-------------------------------------------------+-----------+-------------+-----+
| Push-Benachrichtigungen                         |           |             | X   |
+-------------------------------------------------+-----------+-------------+-----+

Table: Funktionsvergleich mit Konkurrenzprodukten

Neben dem Verwalten und Auswerten von Mitarbeiterskills bietet die
SkillCert-Software einige weitere Funktionen, wie z.B. das erfassen von
Weiterbildungskosten, eine Erinnerungsfunktion bei ablaufenden
Zertifizierungen oder ein Dokumentenmanagement-System.

Mit dem rexx Skillmanagement System lassen sich schnell und
komfortabel Mitarbeiterkompetenzen erfassen, detailliert auswerten und
in Form von Diagrammen anzeigen.

Mit Hilfe von vorher erstellten Anforderungsprofilen ist man in
der Lage, die am besten passenden Mitarbeiter zu ermitteln.

Unsere Anwendung "SMS" umfasst ebenfalls die wesentlichen
Funktionen eines Skillmanagement-Systems. Bietet aber folgende
Alleinstellungsmerkmale:

Der aktuelle Projektstatus der Mitarbeiter wird angezeigt und
bietet so eine Übersicht welcher Mitarbeiter verfügbar ist. Außerdem
wird eine Push-Notification in Form einer E-Mail an das jeweilige Handy
des Consulting Managers versendet, wenn ein Mitarbeiter die gesuchten
Kompetenzen mit ausreichenden Bewertungen gefüllt hat. Das heißt, wenn
eine Suche nach Mitarbeitern mit bestimmten Kompetenzen durch den
Consulting Manager erfolglos war, wird dieser benachrichtigt, sobald es
einen Mitarbeiter gibt, welcher die Suchkriterien erfüllt. Innerhalb der
Projekte existiert zudem eine Übersicht der involvierten
Mitarbeiter.
