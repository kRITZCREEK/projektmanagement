---
documentclass: scrartcl
lang: ngerman
header-includes:
  - \pagestyle{headings}
  - \usepackage{graphicx}
---

\newpage

Zielbestimmung und Zielgruppen
==============================

## Produktperspektive

Es ist eine Anwendung zu erstellen, die das Verwalten, Pflegen und
Auswerten von Kompetenzen der Mitarbeiter eines Beratungsunternehmens
ermöglicht.

Auf diese Weise soll schneller und präziser auf Kundenanfragen reagiert
und Teams mit den nötigen Skillsets gebildet werden können.

Ein besonderer Fokus liegt auf der Mobil- und Offlinefähigkeit der
Anwendung, sodass auch unterwegs und beim Kundeneinsatz auf die
Mitarbeiterinformationen zugegriffen werden kann.

## Einsatzkontext

Das System wird sowohl von Consultants als auch von Consultant Managern
eines Beratungsunternehmens genutzt, um die Einteilung von Mitarbeitern
auf Kundenprojekte zu optimieren und deren Akquise zu unterstützen.

Funktionale Anforderungen
=====

## Produktfunktionen

### *F10* Verwalten der Kompetenzen und Kompetenzarten

Der Consultant Manager kann neue Kompetenzarten erstellen, bestehende
verändern oder löschen.

### *F20* Pflegen der Mitarbeiterprofile

Consultants pflegen selbstständig ihr Mitarbeiterprofil in einem dafür
vorgesehenen Bereich in der Anwendung.

#### *F21* Persönliche Vorstellung ändern

Freitext zur Vorstellung des Consultants sowie fachliche Schwerpunkte und
relevante Erfahrungen

#### *F22* Werdegang und Ausbildung ändern

Angaben zu früheren Beschäftigungen und Positionen sowie
Ausbildungen/Studium.

#### *F23* Zertifikate und Qualifikationen ändern

Angaben zu erworbenen Zertifikaten oder durchgeführten Schulungen und
Auszeichnungen.

#### *F24* Fachvorträge und Publikationen ändern

Angaben zu gehaltenen Fachvorträgen und Veröffentlichungen in
Fachzeitschriften.

### *F30* Pflegen der Mitarbeiterkompetenzen

Consultants verwalten ihre Kompetenzbewertungen selbstständig und auf
Vertrauensbasis. Eine Kompetenzbewertung beinhaltet eine Kompetenzart
und ein Rating.

### *F40* Pflegen des Projektstatus der Consultants

Der Consultant trägt dafür Sorge, dass sein aktueller Projektstatus
gepflegt ist.

### *F50* Suchen von Mitarbeitern nach Kompetenzkriterien

Die Consulting Manager stellen Anfragen an das System und suchen
verfügbare Consultants anhand ihrer Kompetenzen.

### *F60* Bewerten der Eignung eines Mitarbeiters für eine Menge von Kompetenzen

### *F70* Generieren von Mitarbeiterübersichten

Die Consulting Manager können sich Mitarbeiterübersichten zu Werbe- und
Akquisezwecken aus den Mitarbeiterprofilen generieren lassen. Diese sind
ansprechend formatiert, um den Verkauf zu unterstützen.

### *F80* Pflegen der Projektdaten

Consultants und Consultant Manager verwalten gemeinsam den Status und
die Eckdaten der Projekte.

### *F90* Notifikationssystem für neuerworbene Kompetenzen

Falls der Manager bei einer Suche nach bestimmten Kriterien keine
Suchergebnisse findet, kann er eine Notifikation anfordern wenn ein
Mitarbeiter diese Suche durch neuerworbene Kompetenzen erfüllt.

## Produktdaten

- *D10* Mitarbeiterstammdaten

- *D20* Aktueller Projektstatus

- *D30* Kompetenzbewertungen von Mitarbeitern

- *D40* Vergangene Ereignisse und Tätigkeiten

    Projektvergangenheit und Einsatzrollen der Mitarbeiter

- *D50* Zertifikate

- *D60* Vorstellung des Mitarbeiters (Freitext)

- *D70* Relevante Erfahrungen

- *D80* Fachliche Schwerpunkte

- *D90* Fachvorträge und Veröffentlichungen

- *D100* Kompetenzarten

    Kompetenzarten fassen mehrere Kompetenzen zusammen. Kompetenzarten sind
    zum Beispiel:
    
      - Programmiersprachen
      - Social Skills
      - Agiles Vorgehen
      - ...

- *D110* Kompetenzen

    Kompetenzen sind konkrete Fähigkeiten. Beispiele:

      - Java
      - Scrum
      - Präsentieren
      - ...

- *D120* Projekte

- *D130* Gespeicherte Suchanfragen für Notifikationen

## Produktschnittstellen

Das System soll die Mitarbeiterübersichten in einem für handelsübliche
Drucker verständlichem Format exportieren.

Über einen Datenbankadapter können Mitarbeiterstammdaten, Urlaubszeiten,
etc. aus der vorhandenen Unternehmensdatenbank importiert werden.

Für den Notifikationsdienst wird eine Anbindung an einen Mailserver benötigt,
mittels welchem die Notifikationen versendet werden können.

## Anwenderprofile

Das System und dessen Funktionen wird von zwei primären Anwendergruppen,
Consultants und Consultant Managern, verwendet.

-   Consultants

    - F20, F21, F22, F23, F24, F30, F40, F80

-   Consultant Manager

    - F10, F30, F50, F60, F70, F80, F90

# Nicht-funktionale Anforderungen

## Qualitätsanforderungen

-   *T10* Aktualität der Daten gewährleisten

    Die zu erstellende Anwendung synchronisiert sich in regelmäßigen
    Abständen mit der Unternehmensdatenbank. Dieser Prozess kann auch
    manuell von einem Administrator angestoßen werden.
-   *T20* Es können nur Kompetenzen eingetragen werden, die vorher
    durch Manager angelegt wurden
-   *T30* Gute Usability um den Mehraufwand für den Mitarbeiter zu
    minimieren
-   *T40* Schutz der Privatsphäre von Mitarbeitern (Zugriffskontrolle)

    Ein Mitarbeiter kann nicht die Kompetenzen und Bewertungen eines
    anderen Mitarbeiters einsehen. Auf diese kann nur der Consultant
    Manager zugreifen.

-   *T50* Die Mitarbeiter können nur ihr eigenes Profil pflegen
-   *T60* Vollständigkeit des Mitarbeiterprofils

    Der Mitarbeiter soll alle Informationen in sein Profil eintragen
    können, die später für den Export benötigt werden. Kein manuelles
    Nachbearbeiten durch den Consultant Manager ist nötig.

-   *T70* Zuverlässigkeit: Stabilität der Anwendung

    Bei der Projektakquise kann es zu Momenten kommen in denen ein
    Manager ein Ja oder Nein geben muss. In einer solchen Situation ist
    es von großer Wichtigkeit, dass das System erreichbar ist.
-   *T80* Effizienz: Antwortgeschwindigkeit

    Um in Entscheidungssituationen unterstützen zu können muss das
    System über kurze Antwortzeiten (< 5 Sekunden) verfügen.

-   *T90* Parallelität

    Das System hat eine große Anzahl an Nutzern die parallel auf
    es zugreifen. Hierbei muss das System Synchronisationskonflikte
    vermeiden und Antwortzeiten gering halten.

-   *T100* Übertragbarkeit

    Um die verschiedenen Plattformen (vor allem auch Mobile) erreichen
    zu können, muss die Anwendung in den gängigen
    Webbrowsern funktionieren.

-   *T110* Änderbarkeit

    Das Feld der Projektakquise und Skillverwaltung ist in stetem Flux.
    Um auf Neuerungen und veränderte Anforderungen reagieren zu können
    muss das System Ansatzpunkte für Erweiterungen und
    Änderungen bieten.

-   *T120* Korrektheit der Filter

    Die Suche nach Consultants durch den Consultant Manager mittels
    Filterkriterien wie Kompetenzen und Kompetenzbewertungen darf nur
    Mitarbeiter anzeigen, die diese Kriterien auch wirklich erfüllen, um
    peinliche Situationen beim Kunden vermeiden zu können.

## Technische Anforderungen

-   Client-Server Architektur
-   Der Client muss Mobilgeräte sowie Desktopgeräte unterstützen. Der
    Client muss kompatibel mit Internet Explorer, Google Chrome, Firefox
    und Safari sein.
-   Server greift auf die Unternehmensdatenbank zu

# Lieferumfang

Die Verwaltung der Mitarbeiterdaten handhabt der Auftraggeber. Der Auftragnehmer
stellt die Software zur Verfügung und kümmert sich um das Deployment der
fertigen Software.

Es wird ein agiles Vorgehen eingesetzt, dadurch wird der Auftraggeber in
den Entwicklungsprozess eingebunden und erhält regelmäßige Updates über
den Projektstatus.

# Abnahmekriterien

Alle geforderten Funktionen sind umgesetzt und testbar. Die
Qualitätsanforderungen sind erfüllt.

# Anhänge
