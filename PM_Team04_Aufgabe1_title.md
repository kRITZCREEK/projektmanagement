\subject{
    \flushleft
    \vspace{-25mm}
    \hspace{-10mm}
    \includegraphics*[scale=0.2]{TH_Koeln_Logo.png}
    }
\title{
    \vspace{25mm}
    \includegraphics*[scale=0.6]{TH_Koeln_Logo.png}\\
    \vspace{20mm}
    \noindent\makebox[\linewidth]{\rule{\linewidth}{0.4pt}}
    \vspace{-5mm}
    Skill Management System -- S.M.S
    \noindent\makebox[\linewidth]{\rule{\linewidth}{0.4pt}}
}
\subtitle{Aufgabe 1 --- Lastenheft}
\author{Ricarda Jäschke \and Janis Saritzoglou \and Christoph Hegemann
\and Hendrik Bode \and Amir Hossein Babaei \and Fabrice Münn}
\publishers{Projektmanagement WS 15/16 \\ Team 4 }
\maketitle
\newpage
